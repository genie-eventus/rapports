# 1. Mise en contexte

## Présentation équipe de développement

Présenter les membres de l’équipeet leur apport dans le projet

## Description
Décrire ce que fait votre application, dans quel but. D’où vient lebesoin.

## Clientèles visées
Quels sont les types d’usager qui utilisent votre application.Dans quelle proportion ?

# 2. Contenu technique

## Architecture finale du système
Transcrire le diagramme de déploiement aumoment de la démo. Expliquez les modèles d’architectures logiciels utilisés. Ex-pliquer les principaux processus de traitement et lesquelles se font au niveau dela base de données, au niveau de serveur d’application et s’il y a lieu lesquelles sefont au niveau des interfaces usagers.

## Modèle Entité-Relation
Transcrire le Modèle Entité-Relation au moment de la démonstration.

## Persistance
Expliquer quelles méthodes, avec exemple, qu’avez-vous utilisé pourgérer la persistance (myBatis, JDBC, Hibernate)

## Tests et validation
Quelles sont les méthodes que vous avez utilisées pourvalider et tester en tout et en partie le bon fonctionnement de votre base dedonnées, de votre serveur d’application et de vos interfaces usager, s’il y a lieu.

## Problèmes rencontrés
Quels sont les principaux problèmes que vous avez ren-contrés et quels ont été les stratégies pour les gérer.28

## Maintenance à prévoir
Quel genre de maintenance votre application a t-ellebesoin ? Doit-on mettre à jour les serveurs de base de données et d’application ré-gulièrement. Doit-il y avoir de la formation pour les utilisateurs ? Doit-il y avoir dupersonnel technique formé pour s’assurer du bon fonctionnement de l’application ?

# 3. Gestion

## Sommaire de l’avancement
Le sommaire de l’avancement est un résumé desétapes que vous avez franchi pour arriver à la réalisation de votre projet.

## Sommaire de l’effort effectué versus planifié
Faire le récapitulatif du tempstravaillé versus le temps planifié, soit par sprint, par fonctionnalité ou par étapedu projet. N’oubliez pas d’inclure le temps effectué même sur des tâches qui n’ontpas abouti ou qui ont été laissé tomber en chemin.

## Cohésion - travail d’équipe
Répartition du travail parmi les coéquipiers. Quia fait quoi ?

# 4. Démonstration

Faire une démonstration préenregistré de votre application. Vous avez un maximumde 3 minutes de vidéo.

# 5. Contrôle de qualité

## Qualité du code de la base de données
Vous devez démontrer la qualité et la structure de votre code pour des vues, des procédures ainsi que celle des requêtesselect.

## Qualité des autres code
Vous devez démontrer la qualité de votre code desmicro services, des classes d’affaires et de la persistance des données, ainsi que lastructure de votre projet.

## Qualité de l’interface usager
L’ergonomie de l’interface est un aspect impor-tant à considérer et à montrer lors de la présentation.
